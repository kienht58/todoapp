@extends('layout.layout')

@section('title')
Create New Task
@stop

@section('content')
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			{!!Form::open([
					'route'   => 'tasks.store',
					'method'  => 'POST',
					'class'   => 'form-horizontal',
					'enctype' => 'multipart/form-data'
				])
			!!}		
				<div class="form-group">
					{!! Form::label('name', 'Name', [ 'class' => 'control-label' ]) !!}
					{!! Form::text('name', '', [ 'class' => 'form-control', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('description', 'Description', [ 'class' => 'control-label' ]) !!}
					{!! Form::text('description', '', [ 'class' => 'form-control', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('deadline', 'Deadline', [ 'class' => 'control-label' ]) !!}
					{!! Form::date('deadline', '', [ 'class' => 'form-control', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('assign', 'Assign to', [ 'class' => 'control-label' ]) !!}
					{!! Form::select('user_id', $users, [ 'class' => 'form-control', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Create', [ 'class' => 'btn btn-primary pull-right' ])!!}
				</div>
			{!!Form::close()!!}
		</div>
	</div>
@stop