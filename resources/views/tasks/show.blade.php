@extends('layout.layout')

@section('title')
Task {{$task->name}}
@stop

@section('content')
	<h2 style="text-align:center">Task Details</h2>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<ul class="list-group" style="text-align:center">
                <li>Name: {{$task->name}}</li>
                <li>Description: {{$task->description}}</li>
                <li>Deadline: {{$task->deadline}}</li>
                <li>Assgin to: {{$task->assign}}</li>
                <li>Status: {{$task->status}}</li>
                <li>
                	@if(Auth::check())
                		@if(Auth::user()->id == $task->user_id || Auth::user()->isAdmin())
		                	<a href="{{route('tasks.edit', $task->id)}}"><button class="btn btn-info">Edit</button></a>
		                	@if(Auth::user()->isAdmin())
								{!! Form::open([
							        'route' => ['tasks.delete', $task->id],
							        'method' => 'DELETE',
							        'style' =>'display: inline'
							        ])
							    !!}
							        <button class="btn btn-danger">Delete</button>
							    {!! Form::close() !!}
							@endif
					    @endif
				    @endif
                </li>
            </ul>
		</div>
	</div>
	@if($errors->any())
		<h4>{{$errors->first()}}</h4>
	@endif
@stop