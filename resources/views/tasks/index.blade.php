@extends('layout.layout')

@section('title')
Basic Task List
@stop

@section('content')
<div class="row" id="error-container">
@if($errors->any())
	<h4 id="error" style="color:red; padding-left:25px">{{$errors->first()}}</h4>
@endif
</div>
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
	        <div class="panel-heading">
	        </div>
	        <div class="panel-body" id="table-container">
	            <table class="table table-striped table-bordered table-list">
	                <thead>
	                    <tr>
	                        <th class="tble-content">Name</th>
	                        <th class="tble-content">Description</th>
	                        <th class="tble-content">Deadline</th>
	                        <th class="tble-content">Assign to</th>
	                        <th class="tble-content">Status</th>
	                        <th><center><em class="fa fa-cog"></em></center></th>
	                    </tr> 
	                </thead>
			        <tbody>
					    @if($usertasks)
							@foreach($usertasks as $task)
							    <tr style="text-align: center">
							        <td class="tble-content">{{$task->name}}</td>
							        <td class="tble-content">{{$task->description}}</td>
							        <td class="tble-content">{{$task->deadline}}</td>
							        <td class="tble-content">{{$task->username}}</td>
							        <td class="tble-content">{{$task->status}}</td>
							        <td class="tble-content">
							            <a href="{{route('tasks.show', $task->id)}}"><button class="btn btn-primary">Detail</button></a>
										@if($task->status == 0)
											@if(Auth::check())
												@if(Auth::user()->isAdmin() || Auth::user()->id == $task->user_id)
													<a href="{{route('tasks.edit', $task->id)}}"><button class="buttonEditTask btn btn-info">Edit</button></a>
													<button class="buttonCompleteTask btn btn-success" data-id='{{$task->id}}'>Mark as completed</button>
													@if(Auth::user()->isAdmin()) 
														<button class='buttonDeleteTask btn btn-danger' data-id='{{$task->id}}'>Delete</button>
													@endif
												@endif
											@endif
										@endif
									</td>
							    </tr>
							@endforeach
						@endif
			        </tbody>
			 	</table>
	  		</div>
	 	</div>
	 	@if(Auth::check())
	 		@if(Auth::user()->isAdmin())
	 			<a href="{{route('tasks.create')}}"><button class="btn btn-success">Create new task</button></a>
	 		@endif
	 	@endif
	</div>
</div>
<script>
	$(document).ready(function(){
		$('#table-container').on('click', ".buttonDeleteTask", function(e) {
			e.preventDefault();
			var task_id = $(this).attr('data-id');
			var token = '{{ csrf_token() }}';
			$.ajax({
				type: "DELETE",
				url: window.location.href + task_id,
				data: {_token: token},
				success: function() {
					$('#table-container').load(" #table-container");
					$(document).ready();
				},
				error: function() {
					window.location.reload();
				}
			});
		});

		$('#table-container').on('click', ".buttonCompleteTask", function(){
			var token = '{{ csrf_token()}}';
			var task_id = $(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: window.location.href + task_id,
				data: {_token: token},
				success: function() {
					$('#table-container').load(" #table-container");
				},
				error: function() {
					window.location.reload();
				}
			});
		});
	});
</script>
@stop