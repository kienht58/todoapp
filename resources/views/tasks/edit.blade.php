@extends('layout.layout')

@section('title')
Edit Task {{$task->name}}
@stop

@section('content')
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			{!!Form::model($task, [
						'route'   => ['tasks.update', $task->id],
						'method'  => 'POST',
						'class'   => 'form-horizontal',
						'enctype' => 'multipart/form-data'
					])
					!!}	
				<div class="form-group">
					{!! Form::label('name', 'Name', [ 'class' => 'control-label' ]) !!}
					{!! Form::text('name', null, [ 'class' => 'form-control', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('description', 'Description', [ 'class' => 'control-label' ]) !!}
					{!! Form::text('description', null, [ 'class' => 'form-control', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('deadline', 'Deadline', [ 'class' => 'control-label' ]) !!}
					{!! Form::text('deadline', null, [ 'class' => 'form-control', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('assign', 'Assign', [ 'class' => 'control-label' ]) !!}
					{!! Form::text('assign', null, [ 'class' => 'form-control', 'required']) !!}
				</div>

				<div class="form-group">
					{!! Form::submit('Update', [ 'class' => 'btn btn-primary pull-right' ])!!}
				</div>
			{!!Form::close()!!}
		</div>
	</div>
@stop