<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;
use App\Task;

class todoTest extends TestCase
{

    use DatabaseMigrations;

    public function testShouldSeeHeading(){
    	$this->visit('/')
    		 ->see('Simple Todo List')
    		 ->dontSee('Laravel 5');
    }

    public function testShouldSeeCreateTaskForm(){
    	$this->visit('/')
    		 ->click('Create new task')
    		 ->seePageIs('/new');
    }

    public function testShouldCreateNewTask(){
        $this->visit('/new')
             ->type('tasktask', 'name')
             ->type('desdes', 'description')
             ->type('2016-6-6', 'deadline')
             ->type('Kien', 'assign')
             ->press('Create')
             ->seeInDatabase('tasks', ['name' => 'tasktask']);
    }

    public function testShouldRedirectAfterSuccessfullyCreated() {
        $response = $this->call('POST', '/', [
            'name' => 'task 69696969',
            'description' => 'description',
            'deadline' => '2016-6-6',
            'assign' => 'Kien',
            '_token' => '{{ csrf_token() }}'
            ]);

        $this->assertEquals(302, $response->status());
        $this->visit('/')
             ->see('task 69696969');
    }

    public function testShouldEditTask() {
        $random_task = Task::orderBy(DB::raw('RAND()'))->first();
        $this->visit('/'.$random_task->id.'/edit')
             ->type('edited task', 'name')
             ->press('Update')
             ->seePageIs('/'.$random_task->id)
             ->see('edited task');
    }

    public function testShouldRedirectToShowAfterEdited() {
        $task = factory(App\Task::class)->create([
            'name' => 'task',
            'description' => 'description',
            'deadline' => '',
            'assign' => 'Kien'
            ]);

        $response = $this->call('POST', '/'.$task->id, [
            'name' => 'edited task'
            ]);

        $this->assertEquals(302, $response->status());

        $this->visit('/'.$task->id)
             ->see('edited task');
    }

    public function testShouldDeleteTask() {
        $random_task = Task::orderBy(DB::raw('RAND()'))->first();
        $name = $random_task->name;
        $this->visit('/'.$random_task->id)
             ->press('Delete')
             ->seePageIs('/')
             ->dontSee('$name');
    }

  	public function testShouldChangeTaskStatus() {
  		
  	}

  	public function testShouldHideButtonWhenTaskIsCompleted() {

  	}

  	public function testAjaxShouldWorkCorrectly() {

  	}
}