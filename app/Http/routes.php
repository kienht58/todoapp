<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@logout');

Route::get('register', 'Auth\AuthController@showRegistrationForm');
Route::post('register', 'Auth\AuthController@doRegister');

Route::group(['middleware' => 'auth'], function() {
	
	Route::get('/new', [
		'as' => 'tasks.create',
		'uses' => 'TasksController@taskCreate'
		]);

	Route::post('/', [
		'as' => 'tasks.store',
		'uses' => 'TasksController@taskStore'
		]);

	Route::get('/{task_id}/edit', [
		'as' => 'tasks.edit',
		'uses' => 'TasksController@taskEdit'
		]);

	Route::post('/{task_id}', [
		'as' => 'tasks.update',
		'uses' => 'TasksController@taskUpdate'
		]);

	Route::delete('/{task_id}', [
		'as' => 'tasks.delete',
		'uses' => 'TasksController@taskDelete'
		]);
});

Route::get('/', [
		'as' => 'tasks.index', 
		'uses' => 'TasksController@taskIndex'
		]);

Route::get('/{task_id}', [
		'as' => 'tasks.show',
		'uses' => 'TasksController@taskShow'
		]);