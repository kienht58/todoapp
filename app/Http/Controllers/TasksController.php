<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Requests;
use Carbon\Carbon;
use Response;
use App\Task;
use App\User;

class TasksController extends Controller
{
    public function taskIndex(){
        $usertasks = DB::table('tasks')
                        ->join('users', 'tasks.user_id', '=', 'users.id')
                        ->get();
    	$tasks = Task::all();

    	return view('tasks.index', compact('usertasks')); 
    }

    public function taskCreate(){
        if(Auth::user()->isAdmin()) {
            $users = User::where('role', 'user')->lists('username', 'id');
        	return view('tasks.new', compact('users'));
        }

        return back()->withErrors(['msg' => 'You do not have permission to do this!']);
    }

    public function taskStore(Request $request){
        if(Auth::user()->isAdmin()) {
            $request->deadline = Carbon::createFromFormat('Y-m-d', $request->deadline);
        	$data = $request->except('_method', '_token');
        	$task = Task::create($data);
            $task->user_id = Auth::user()->id;

        	return redirect()->route('tasks.index');
        }

        return back()->withErrors(['msg' => 'You do not have permission to do this!']);
    }

    public function taskShow($task_id) {
    	$task = Task::where('id', $task_id)->first();

    	return view('tasks.show', compact('task'));
    }

    public function taskEdit($task_id) {
    	$task = Task::where('id', $task_id)->first();
        if(Auth::user()->id == $task->user_id || Auth::user()->isAdmin()){
            if($task->status == 0){
                return view('tasks.edit', compact('task'));
            }
            
            return back()->withErrors(['msg' => 'You can not edit completed task!']);
        }
        else 
            return back()->withErrors(['msg' => 'You do not have permission to do this!']);
    }

    public function taskUpdate(Request $request, $task_id) {
    	$task = Task::where('id', $task_id)->first();
        if(Auth::user()->id == $task->user_id or Auth::user()->isAdmin()){
            if($task->status == 0) {
                if($request->ajax()) {
                    $task->update(['status' => 1]);

                    return "update complete";
                }
                $task->update($request->except('_method', '_token'));

                return redirect()->route('tasks.show', compact('task_id'));
            }
            
            return back()->withErrors(['msg' => 'You do not have permission to do this!']);
        }
        else 
            return back()->withErrors(['msg' => 'You do not have permission to do this!']);
    }

    public function taskDelete(Request $request, $task_id) {
        $task = Task::where('id', $task_id)->first();
        if(Auth::user()->isAdmin()){
            if($request->ajax()){
                $task->delete();
                return "delete complete";
            }
            $task->delete();
            return redirect()->route('tasks.index');
        }
        else 
            return back()->withErrors(['msg' => 'You do not have permission to do this!']);
    }
}
