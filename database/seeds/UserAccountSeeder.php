<?php

use Illuminate\Database\Seeder;
use App\User;

class UserAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'username' => 'admin',
        	'email' => 'admin@example.com',
        	'password' => bcrypt('123123'),
            'role' => 'admin'
        ]);

        User::create([
            'username' => 'user1',
            'email' => 'user1@example.com',
            'password' => bcrypt('123123'),
            'role' => 'user'
        ]);

        User::create([
            'username' => 'user2',
            'email' => 'user2@example.com',
            'password' => bcrypt('123123'),
            'role' => 'user'
        ]);
    }
}
